import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

const appRoutes: Routes = [
    { path: 'edit', loadChildren: 'app/modules/editor/editor.module#EditorModule' },
    { path: 'auth', loadChildren: 'app/modules/auth/auth.module#AuthModule' },
    { path: '', loadChildren: 'app/modules/main/main.module#MainModule' },
    { path: '', redirectTo: '/', pathMatch: 'full'} // default route

    // { path: '**', loadChildren'app/modules/notFound/notFound.module#notFoundModule'}
  ];

@NgModule({
  imports: [
    RouterModule.forRoot(
        appRoutes,
        {   // enableTracing: true, // <-- debugging purposes only
            preloadingStrategy: PreloadAllModules  // SelectivePreloadingStrategy
        })
  ],
  exports: [ RouterModule ],
  providers: [ ]
})

export class AppRoutingModule {}
