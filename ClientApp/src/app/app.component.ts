import { Component, OnInit, OnDestroy } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent  implements OnInit, OnDestroy {
    title = 'ClientApp';

    private subscriptions = [];
    public update = false;

    constructor(private updates: SwUpdate) {}

    ngOnInit(): void {
        const updatesSubscription = this.updates.available
            .subscribe(events => {
                // this.update = true;
                // TODO Maybe just warn the user about an update rather than automatically update?
                this.updates.activateUpdate()
                    .then(() => {
                        document.location.reload();
                    });
            });
        this.subscriptions.push(updatesSubscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}
