export class  IAttachment {
    id: string;
    format: string;
    sizeMB: number;
    offline: boolean;
}

export class  Attachment implements IAttachment {
    id = '';
    format = 'pdf';
    sizeMB = 0;
    offline = false;

    constructor(instanceData?: Attachment) {
        if (instanceData) {
          this.deserialize(instanceData);
        }
      }

    private deserialize(instanceData: Attachment) {
        // Note this.active will not be listed in keys since it's declared, but not defined
        const keys = Object.keys(this);

        for (const key of keys) {
            if (instanceData.hasOwnProperty(key)) {
                this[key] = instanceData[key];
            }
        }
    }
}
