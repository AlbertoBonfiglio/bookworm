export interface  IAuthor {
    firstName: string;
    middleName: string;
    lastName: string;
    titles: Array<string>;
}

export class  Author implements IAuthor {
    firstName = '';
    middleName = '';
    lastName = '';
    titles = [];

    constructor(instanceData?: Author) {
        if (instanceData) {
          this.deserialize(instanceData);
        }
      }

    private deserialize(instanceData: Author) {
        // Note this.active will not be listed in keys since it's declared, but not defined
        const keys = Object.keys(this);

        for (const key of keys) {
            if (instanceData.hasOwnProperty(key)) {
                this[key] = instanceData[key];
            }
        }
    }
}
