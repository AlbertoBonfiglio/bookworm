import { Attachment } from './attachment';
import { Author } from './author';

export interface IItem {
    _id: string;
    _rev: string;
    userId: string;
    type: string;
    isbn: string;
    title: string;
    genres: string[];
    series: string[];
    keywords: string[];
    dateAdded: string;
    publishers: string[];
    datePublished: string;
    languages: string[];
    files: Attachment[];
    authors: Author[];

}


export class Item implements IItem {
    _id = '';
    _rev = '';
    userId: '';
    type = 'item';
    isbn = '1234567890123';
    title = 'The hitchhicker\'s guide to the galaxy';
    genres = [];
    series = [];
    keywords = [];
    dateAdded = null;
    publishers =  [];
    datePublished = null;
    languages = [];
    files: Attachment[] = [];
    authors: Author[] = [];

    constructor(instanceData?: Item) {
        if (instanceData) {
          this.deserialize(instanceData);
        }
      }

    private deserialize(instanceData: Item) {
        // Note this.active will not be listed in keys since it's declared, but not defined
        const keys = Object.keys(this);

        for (const key of keys) {
            if (instanceData.hasOwnProperty(key)) {
                this[key] = instanceData[key];
            }
        }
    }

    /*
    public get attachments(): Attachment[] {
        return this.files;
    }

    public set attachments(data: Attachment[]) {
        this.files = data.map(attachment => new Attachment(attachment));
    }

    public get authors(): Author[] {
        return this._authors;
    }

    public set authors(data: Author[]) {
        this._authors = data.map(author => new Author(author));
    }
    */

}
