import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';

import { MainAuthComponent } from './index';

@NgModule({
    imports: [
        CommonModule,
        AuthRoutingModule
    ],
    declarations: [ MainAuthComponent ]
})
export class AuthModule { }
