import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainEditorComponent} from './index';

const routes: Routes = [
    { path: '',  component: MainEditorComponent }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
  })

  export class EditorRoutingModule {}
