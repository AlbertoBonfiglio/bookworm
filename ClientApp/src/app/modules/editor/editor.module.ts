import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorRoutingModule } from './editor-routing.module';
import { MainEditorComponent } from './index';

@NgModule({
  imports: [
    CommonModule,
    EditorRoutingModule
  ],
  declarations: [MainEditorComponent]
})
export class EditorModule { }
