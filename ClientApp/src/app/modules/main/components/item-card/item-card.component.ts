import { Item } from './../../../../models/item';
import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {

  private _data = new BehaviorSubject<Item>(null);

  @Input('data')
    set data(value: any) { this._data.next(value); }
    get data() { return this._data.getValue(); }

  constructor() { }

  ngOnInit() {
  }

  onSelectItem() {
    const item: Item = this._data.getValue();
    window.alert(`You selected ${item._id}`);
  }

  onOpenItem() {
    const item: Item = this._data.getValue();
    window.alert(`You opened ${item._id}`);
  }
}
