import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

import { PouchdbService } from './../../../../services/pouchdb/pouchdb.service';
import { Item } from './../../../../models/item';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit, OnDestroy {

    private _pagesize = 10;
    @Input()
    set pageSize(size: number) {
        this._pagesize = size;
    }

    private subscriptions = [];
    private _dataSubscription: Subscription;
    data$: BehaviorSubject<Array<Item>> = new BehaviorSubject<Array<Item>>(null);


    SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };

    constructor(private db: PouchdbService, private router: Router) { }

    public ngOnInit() {
        this.getData();
    }

    private getData() {
        this._dataSubscription = this.db.fetchPage(null, this._pagesize)
        .subscribe(
            data => {
                this.data$.next(data);
            },
            error => {
                console.log('Error retrieving items from storage: ', error);
            }
        );

    }

    public addItem() {
        /*
        const maxZ =  Array.from(document.querySelectorAll('body *'))
            .map(a => parseFloat(window.getComputedStyle(a).zIndex))
            .filter(a => !isNaN(a))
            .sort((a, b) => a - b)
            .pop();
        */
        // window.alert(`Adding new item! BTW the MaxZ is ${maxZ}`);
        this.router.navigate(['edit']);
    }

    public deleteItem(item: Item) {
        window.alert(`Are you sure you want to delete these?`);

        this.db.delete(item._id).subscribe(
            result => {
                const index = this.data$.getValue().indexOf(item);
                if (index !== -1) {
                    this.data$.getValue().splice(index, 1);
                }
          },
          error => {
            console.log('Error deleting document: ', error);
          }
        );
    }


    public editItem() {
        window.alert(`Are you sure you want to edit these?`);
    }

    public onScroll() {
        console.log('Scrolling');
        if (this._dataSubscription ) {
            // Clean up just in case;
            // TODO investigate one of these techniques
            // https://medium.com/@maciekprzybylski/remember-to-unsubscribe-from-streams-in-your-angular-components-caf0bedd6ac2
            this._dataSubscription.unsubscribe();
        }

        const dataArray = this.data$.getValue();
        if (dataArray.length > 0) {
            this.db.fetchPage(dataArray[dataArray.length - 1]._id , this._pagesize)
                .subscribe(
                    data => {
                        if (data && data.length > 0) {
                            this.data$.next(dataArray.concat(data));
                        } else {
                            console.log('No more data retrieved');
                        }
                    },
                    error => { console.log(error); }
                );
        } else {
            this.getData();
        }
    }


    onSwipeLeft(event: any) {
        console.log('swiped left!');
    }

    onSwipeRight(event: any ) {

    }


    private fake() {
        const n = 0;
        const retval = [];
        while (n < 50 ) {
            const v4 = require('uuid/v4');
            const id = v4();
            const attachmentId = v4();
            const doc = {
                'userId': 'xyz',
                'type': 'item',
                'isbn:': '1234567890123',
                'title': 'The hitchhicker\'s guide to the galaxy',
                'authors': [
                    {'firstName': 'Douglas', 'lastName': 'Adams'}
                ],
                'genres': ['Science Fiction'],
                'series': ['The hitchhicker\'s guide to the galaxy'],
                'keywords': ['Comedy', 'SciFi'],
                'dateAdded': null,
                'publisher': ['Addison Wesley'],
                'datePublished': null,
                'languages': ['us-en'],
                'attachments': [
                    {   'id': attachmentId,
                        'format': 'pdf',
                        'sizeMB': 1024,
                        'offline': false
                    }
                ]
            };
            retval.push(doc);
        }
        return retval;
    }

    public ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}
