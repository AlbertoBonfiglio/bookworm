import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';

import { ItemListComponent, ItemMetadataComponent } from './components/itemIndex';

const routes: Routes = [
    { path: '',  component: MainComponent,
        children: [
            { path: '', component: ItemListComponent},
            { path: 'addItem', component: ItemMetadataComponent}
        ]
    }
/*
    ,
    { path: 'signUp',
        loadChildren: 'app/modules/subscription/subscription.module#SubscriptionModule',
        canActivate: []
    },

    { path: 'signUp/:id',
        loadChildren: 'app/modules/subscription/subscription.module#SubscriptionModule',
        resolve: {pricingTier: PricingTierResolverService}},

    { path: 'dashboard',
        loadChildren: 'app/modules/dashboard/dashboard.module#DashboardModule',
        canActivate: [LoggedInUserGuard]
    },

    { path: 'dashboard/:id',
        loadChildren: 'app/modules/dashboard/dashboard.module#DashboardModule',
        canActivate: [LoggedInUserGuard]
    }
*/
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
  })

  export class MainRoutingModule {}
