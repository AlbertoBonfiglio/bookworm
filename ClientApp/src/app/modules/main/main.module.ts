import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from './../../../environments/environment.prod';


// import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
// import { NgZorroAntdMobileModule } from 'ng-zorro-antd-mobile';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { MatCardModule, MatIconModule, MatToolbarModule, MatButtonModule } from '@angular/material';
// import { FlexLayoutModule} from '@angular/flex-layout';

import { MainComponent } from './components/main/main.component';
import { MainRoutingModule } from './main-routing.module';

import { PouchdbService } from './../../services/pouchdb/pouchdb.service';
import { BlobStoreService } from './../../services/blobStore/blob-store.service';

import { ItemCardComponent, ItemMetadataComponent, ItemListComponent } from './components/itemIndex';

import 'hammerjs';

@NgModule({
    imports: [
        CommonModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireStorageModule,
//        FormsModule,
        HttpClientModule,
//        NgZorroAntdModule,
//        NgZorroAntdMobileModule,
//        FlexLayoutModule,
        InfiniteScrollModule,
        MatButtonModule,
        MatCardModule,
        MatIconModule, MatToolbarModule,
        MainRoutingModule
    ],

    declarations: [
        MainComponent,
        ItemCardComponent,
        ItemMetadataComponent,
        ItemListComponent],

    exports: [ InfiniteScrollModule ],

    providers: [
        PouchdbService,
        BlobStoreService // ,
  //      { provide: NZ_I18N, useValue: en_US }
    ],

    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class MainModule { }
