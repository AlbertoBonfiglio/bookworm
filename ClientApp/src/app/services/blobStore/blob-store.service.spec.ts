import { TestBed } from '@angular/core/testing';

import { BlobStoreService } from './blob-store.service';

describe('BlobStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlobStoreService = TestBed.get(BlobStoreService);
    expect(service).toBeTruthy();
  });
});
