import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class BlobStoreService {

  constructor(private storage: AngularFireStorage) {
      console.log('Storage:', this.storage.storage.app );
   }


}
