import { Injectable, EventEmitter } from '@angular/core';
import { BlobStoreService } from '../blobStore/blob-store.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { from } from 'rxjs';
import { Item } from '../../models/item';
import PouchDB from 'pouchdb-browser';

// const PouchDB: typeof Pouch = require('pouchdb');
// import * as PouchDB from 'pouchdb-browser';

@Injectable({  providedIn: 'root' })
export class PouchdbService {
    private isInstantiated = false;
    private localDB: any;
    private listener: EventEmitter<any> = new EventEmitter();

    private options = {include_docs: true, limit: 8, skip: 0, startkey: null };

    documentCount$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    constructor( private blobStore: BlobStoreService ) {
        if (!this.isInstantiated) {
            this.localDB = new PouchDB('library');
            this.isInstantiated = true;
        }
        this.documentCount$.next(this.getDBDocumentCount());

        this._seed();
    }

    public fetch(): Observable<Array<Item>> {
        return from(
            this.localDB
                .allDocs({include_docs: true})
                .then( resultOdAllDocs => {
                    const data = resultOdAllDocs.rows.map( row => <Item>row.doc );
                    return data;
                })
                .then(this.documentCount$.next(this.getDBDocumentCount()))
        );
    }

    public fetchPage(startKey = null, pageSize = 10): Observable<Array<Item>> {
        this.options.limit = pageSize;
        this.options.startkey = startKey;

        if (startKey === null) {
            this.options.skip = 0; // for new queries
        }

        return from(
            this.localDB
                .allDocs(this.options, (err, response) => {
                    if (response && response.rows.length > 0) {
                        this.options.startkey = startKey;
                        this.options.skip = 1;
                    }
                })
                .then( resultOdAllDocs => {
                    const data = resultOdAllDocs.rows.map( row => <Item>row.doc );
                    return data;
                })
                .then(this.documentCount$.next(this.getDBDocumentCount()))
        );
    }


    public get(id: string): Observable<any> {
        return from(
            this.localDB
                .get(id)
                .then( resultOfGet => resultOfGet)
                .catch(error => {
                    console.error('Error getting document', error);
                })
        );
    }

    public put(id: string, document: any): Observable<any> {
        document._id = id;

        return from (
            this.localDB.get(id)
                .then( resultOfGet => {
                    document._rev = resultOfGet._rev;
                    return this.localDB.put(document);
                    },
                    error => {
                        if ( error.status === 404) {
                            return this.localDB.put(document);
                        } else {
                            return new Promise(
                                (resolve, reject) => { reject(error); }
                            );
                        }
                    }
                )
                .then(resultOfPut => resultOfPut)
                .catch( error => error )
                .then(this.documentCount$.next(this.getDBDocumentCount()))
        );
    }

    public putAttachment(docId, attachmentId, revision, attachment, type): Observable<any> {
        return from(
            this.localDB
                .putAttachment(docId, attachmentId, revision, attachment, type)
                .then( resultOfPutAttachment => {
                    console.log('success', resultOfPutAttachment);
                    return resultOfPutAttachment;
                })
                .catch(error => {
                    console.log('Error Putting Attachment', error);
                    return error;
                })
            );
        }

    public getAttachment(docId, attachmentId): Observable<any> {
        return from(
            this.localDB.getAttachment(docId, attachmentId)
            .then( resultOfGetAttachment => resultOfGetAttachment )
            .catch( error => error )
        );
    }

    public getAttachmentFromRevision(docId, attachmentId, revision): Observable<any> {
        return from(
            this.localDB.getAttachment(docId, attachmentId, {'rev': revision})
            .then( resultOfGetAttachment => resultOfGetAttachment )
            .catch( error => error )
        );
    }

    public delete(id: string): Observable<any> {
        return from(
            this.localDB.get(id)
            .then( resultOfGet => {
                this.localDB.remove(resultOfGet._id, resultOfGet._rev);
                })
                .catch(error => {
                    console.error('Error getting document', error);
                })
        );
    }

    public sync(remote: string) {
        const remoteDB = new PouchDB(remote);
        this.localDB
            .sync(remoteDB, {
                live: true
            })
            .on('change', change => {
                this.listener.emit(change);
            })
            .then(this.documentCount$.next(this.getDBDocumentCount()));
    }

    public getChangeListener() {
        return this.listener;
     }


    // Returns the total document count.
    // If an error occurs returns -1 and logs the errot to the console
    private getDBDocumentCount(): number {
        return this.localDB.info()
            .then( info =>  info.doc_count )
            .catch( error => {
                console.log(console.error());
                return -1;
            });
    }

    private _seed() {
        this.localDB.info()
            .then(
                info => {
                    if (info.doc_count > 100 ) { return; }

                    const v4 = require('uuid/v4');
                    const id = v4();
                    const attachmentId = v4();
                    const doc = {
                        'userId': 'xyz',
                        'type': 'item',
                        'isbn:': '1234567890123',
                        'title': 'The hitchhicker\'s guide to the galaxy',
                        'authors': [
                            {'firstName': 'Douglas', 'middleName': 'A.', 'lastName': 'Adams', 'titles' : []}
                        ],
                        'genres': ['Science Fiction'],
                        'series': ['The hitchhicker\'s guide to the galaxy'],
                        'keywords': ['Comedy', 'SciFi'],
                        'dateAdded': null,
                        'publisher': ['Addison Wesley'],
                        'datePublished': null,
                        'languages': ['us-en'],
                        'halepuppappera': true,
                        'attachments': [
                            {   'id': attachmentId,
                                'format': 'pdf',
                                'sizeMB': 1024,
                                'offline': false
                            }
                        ]
                    };

                    this.put(id, doc )
                        .subscribe(
                            resultOfPut => {
                                console.log('Seed:', resultOfPut);
                                const blob = new Blob(['Is there life on Mars?'], {type: 'text/plain'});
                                this.putAttachment(resultOfPut.id, attachmentId, resultOfPut.rev, blob, 'text/plain')
                                    .subscribe(resultOfPutAttachment => {
                                                    console.log(resultOfPutAttachment);
                                                    this.getAttachment(resultOfPutAttachment.id, attachmentId)
                                                        .subscribe(resultOfGetAttachment => console.log(resultOfGetAttachment));
                                                }
                                    );
                            },
                        error => {
                            console.error('Error adding attachment', error);
                            console.log('Document: ', doc);
                        }
                    );

                }
            )
            .then(
                this.documentCount$.next(this.getDBDocumentCount())
            );
    }

}
