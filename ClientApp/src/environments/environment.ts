// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyApoT_yIuK8CyoRUbJkd5-oIg578caNkL8',
    authDomain: 'bonfisoft-bookworm.firebaseapp.com',
    databaseURL: 'https://bonfisoft-bookworm.firebaseio.com',
    projectId: 'bonfisoft-bookworm',
    storageBucket: 'bonfisoft-bookworm.appspot.com',
    messagingSenderId: '130717210239'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
